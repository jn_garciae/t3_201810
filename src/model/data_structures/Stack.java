package model.data_structures;



public class Stack<E> implements IStack {

	private class StackNode<E>{

		E item;

		StackNode<E> next;
	}

	private StackNode<E> first = null;

    
	public void push(Object item) {
		StackNode<E> oldNode = first;
		first = new StackNode<E>();
		first.item = (E)item;
		first.next = oldNode;
		
	}

	@Override
	public Object pop() {
	
		E item = (E) first.item;
		first = first.next;
		return item;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first == null;
	}

}
