package model.data_structures;


public class Queue<E>implements IQueue{

 
	private class QueueNode<E>{
		
		E item;
		
		QueueNode<E> next;
		
	}
	
	public class Iterator<E>{
		
		E item;
	    QueueNode<E> actual;
	
		
		
		public Iterator(QueueNode E){
			
			actual = E;
			
		}
		
		public boolean hasNext(){
			return actual == null;
		}
		
		public E next() throws Exception{
			if ( actual == null )
			 { throw new Exception ("No hay siguiente"); }
			 E elemento = actual.item; // ultimo elemento visitado
			 actual = actual.next;
			 return elemento;

		}
		
		
	}
	
	private QueueNode<E> first, last;
	
	

	@Override
	public void enqueue(Object item) {
		// TODO Auto-generated method stub
		 QueueNode<E> oldlast = last;
		 last = new QueueNode<E>();
		 last.item = (E) item;
		 last.next = null;
		 if (isEmpty()) first = last;
		 else oldlast.next = last; 
	}

	@Override
	public Object dequeue() {
		 E item = first.item;
		 first = first.next;
		 if (isEmpty()) last = null;
		 return item;
		 
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first == null;
	}

	@Override
	public int size() {
		
		return 0;
	}
	public Iterator getIterator(){
		
		Iterator<E> iterator = new Iterator<E>(first);
		
		return iterator;
	}

	

	
	
	
	
	
	
}
	



	