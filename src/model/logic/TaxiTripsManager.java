package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

import api.ITaxiTripsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Taxi;
import model.vo.Service;

public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 

	public void loadServices(String serviceFile, String taxiId) {
		// TODO Auto-generated method stub

		System.out.println("Inside loadServices with File:" + serviceFile);
		System.out.println("Inside loadServices with TaxiId:" + taxiId);
		try
		{
			JsonReader jr = new JsonReader(new FileReader(serviceFile));
			Gson gs = new GsonBuilder().create();

			jr.beginArray();
			while(jr.hasNext())
			{
				Service s = gs.fromJson(jr, Service.class);
				if(s.getTaxiId().equals(taxiId)){
					
						Queue<Service> serviciosCola = null;
						serviciosCola.enqueue(s);	
					
					
						Stack<Service> serviciosPila = null;
						serviciosPila.push(s);
				}
			}
			jr.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();

		}
	}

	@Override
	public int [] servicesInInverseOrder() {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInInverseOrder");
		Stack<Service> servicios = null;
		
		
		int [] resultado = new int[2];
		return resultado;
	}

	@Override
	public int [] servicesInOrder(String taxiId) {
		// TODO Auto-generated method stub
		System.out.println("Inside servicesInOrder");
		Queue<Service> servicios = null;
		loadServices("/data/taxi-trips-wrvz-psew-subset-medium.json", taxiId);
		Iterator<Service> i = (Iterator<Service>) servicios.getIterator();
		Service s = (Service) i;
		int x = 0;
		while(i.hasNext())
		{
			Service n = i.next();
			if(s.getTripStartTime().compareTo(n.getTripStartTime())<0)
			{
				x++;
			}
			
		}
		int [] resultado = new int[x];
		return resultado;
		
	}


}
